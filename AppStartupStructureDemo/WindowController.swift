//
//  WindowController.swift
//  AppStartupStructureDemo
//
//  Created by Alexander Momchilov on 2021-02-13.
//

import AppKit

class WindowController: NSWindowController {
	@IBOutlet var viewModeSegmentedControl: NSSegmentedControl!
	
	override func windowDidLoad() {
		print("The initial view mode is \(tooltipOfSelectedViewMode())")
		
		self.contentVC.updateSelectedViewMode(to: tooltipOfSelectedViewMode())
	}
	
	@IBAction func viewModeChanged(_ sender: NSSegmentedControl) {
		print("The view mode changed to \(tooltipOfSelectedViewMode())")
		
		self.contentVC.updateSelectedViewMode(to: tooltipOfSelectedViewMode())
	}
	
	/// Using the tooltips to tell these apart visually. Scrappy, but only for demo.
	private func tooltipOfSelectedViewMode() -> String {
		let selectedIndex = viewModeSegmentedControl.selectedSegment
		return viewModeSegmentedControl.toolTip(forSegment: selectedIndex)!
	}
	
	/// Just a convenience property that tucks away the forced downcast
	private var contentVC: ViewController {
		self.contentViewController as! ViewController
	}
}

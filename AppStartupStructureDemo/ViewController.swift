//
//  ViewController.swift
//  AppStartupStructureDemo
//
//  Created by Alexander Momchilov on 2021-02-13.
//

import Cocoa

class ViewController: NSViewController {

	@IBOutlet var selectedViewModeTextField: NSTextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()

		// ℹ️ No access to the initial selected view mode, because self.window is not yet set.
	}
	
	/// Just a string, for simplicity's sake.
	func updateSelectedViewMode(to newMode: String) {
		self.selectedViewModeTextField.stringValue = newMode
	}
	
	/// Just a string, for simplicity's sake.
	func updateSelectedViewMode(to newMode: String) {
		self.selectedViewModeTextField.stringValue = newMode
	}
}


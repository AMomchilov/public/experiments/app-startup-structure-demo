Compare [`master/AppStartupStructureDemo/ViewController.swift`](https://gitlab.com/AMomchilov/public/experiments/app-startup-structure-demo/-/blob/master/AppStartupStructureDemo/ViewController.swift) and [`solution-2-using-FRP/AppStartupStructureDemo/ViewController.swift`](https://gitlab.com/AMomchilov/public/experiments/app-startup-structure-demo/-/blob/solution-2-using-FRP/AppStartupStructureDemo/ViewController.swift).

In the conventional imperative style, the window controller can send messages to the view controller when the toolbar buttons are pressed. No problem.

But in the FRP style, the connection is expressed as a Publisher that's passed down from the window controller to the view controller. However, the `windowDidLoad` of the window runs after all of its views are done. So the view can't rely on this publisher being available in its `viewDidLoad`. I could workaround this by moving my initialization logic to `viewDidAppear`, but that feels jank.
